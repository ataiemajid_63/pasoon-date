/**
 *
 * @param {number} timestamp
 * @param {number} offset
 */
var PasoonDate = function (timestamp, offset) {

    var J1970 = 2440587.5;                // Julian date at Unix epoch: 1970-01-01

    var _julianDayNumber;
    var _offset;
    var _hour;
    var _minute;
    var _second;

    var _gregorianDate;
    var _islamicDate;
    var _shiaDate;
    var _jalaliDate;

    /**
     *
     * @param timestamp
     */
    var setTimestamp = function (timestamp) {
        setJulianDayNumber(new Number(J1970 + Math.floor(timestamp / (60 * 60 * 24))));
        var time = julianTimeToTime(new Number(J1970 + (timestamp / (60 * 60 * 24))));
        _hour = time[0];
        _minute = time[1];
        _second = time[2];
    };

    /**
     *
     * @returns {number} timestamp
     */
    var getTimestamp = function () {
        var timestamp = (getJulianDayNumber() - J1970) * (60 * 60 * 24 * 1000);
        return (_hour * 60 * 60 + _minute * 60 + _second) + Math.round(timestamp / 1000);
    };

    /**
     *
     * @param julianDayNumber
     */
    var setJulianDayNumber = function (julianDayNumber) {
        _julianDayNumber = julianDayNumber;
    };

    /**
     *
     * @return {number} Julian Day Number
     */
    var getJulianDayNumber = function () {
        return _julianDayNumber;
    };

    /**
     *
     * @param {number} offset seconde
     */
    var setOffset = function (offset) {
        _offset = offset;
    };

    /**
     *
     * @return {number} offset in seconde
     */
    var getOffset = function () {
        return _offset;
    }

    /**
     *
     * @param julianDayNumber
     * @return {number[]} [hour, minute, second]
     */
    var julianTimeToTime = function (julianDayNumber) {
        julianDayNumber += 0.5;
        /* Astronomical to civil */
        var ij = ((julianDayNumber - Math.floor(julianDayNumber)) * 86400.0) + 0.5;
        return [
            Math.floor(ij / 3600),
            Math.floor((ij / 60) % 60),
            Math.floor(ij % 60)
        ];
    };

    /**
     *
     * @param a
     * @param b
     * @return {number}
     */
    var mod = function (a, b) {
        return a - (b * Math.floor(a / b));
    };

    /**
     *
     * @param julianDayNumber
     * @return {number}
     */
    var dayOfWeek = function (julianDayNumber) {
        return mod(Math.floor((julianDayNumber + 1.5)), 7);

    };

    /**
     *
     * @param year
     * @param month
     * @param day
     * @param hours
     * @param minute
     * @param second
     * @return {GregorianDate}
     */
    var gregorian = function (year, month, day, hours, minute, second) {
        if (_gregorianDate === undefined || year) {
            _gregorianDate = new GregorianDate(year, month, day, hours, minute, second);
            _gregorianDate.islamic = islamic;
            _gregorianDate.shia = shia;
            _gregorianDate.jalali = jalali;
        }
        return _gregorianDate;
    };

    /**
     *
     * @param year
     * @param month
     * @param day
     * @param hours
     * @param minute
     * @param second
     * @return {IslamicDate}
     */
    var islamic = function (year, month, day, hours, minute, second) {
        if (_islamicDate === undefined || year) {
            _islamicDate = new IslamicDate(year, month, day, hours, minute, second);
            _islamicDate.gregorian = islamic;
            _islamicDate.shia = shia;
            _islamicDate.jalali = jalali;
        }
        return _islamicDate;
    };

    /**
     *
     * @param year
     * @param month
     * @param day
     * @param hours
     * @param minute
     * @param second
     * @return {ShiaDate}
     */
    var shia = function (year, month, day, hours, minute, second) {
        if (_shiaDate === undefined || year) {
            _shiaDate = new ShiaDate(year, month, day, hours, minute, second);
            _shiaDate.islamic = islamic;
            _shiaDate.gregorian = gregorian;
            _shiaDate.jalali = jalali;
        }
        return _shiaDate;
    };

    /**
     *
     * @param year
     * @param month
     * @param day
     * @param hours
     * @param minute
     * @param second
     * @return {JalaliDate}
     */
    var jalali = function (year, month, day, hours, minute, second) {
        if (_jalaliDate === undefined || year) {
            _jalaliDate = new JalaliDate(year, month, day, hours, minute, second);
            _jalaliDate.gregorian = gregorian;
            _jalaliDate.shia = shia;
            _jalaliDate.islamic = islamic;
        }
        return _jalaliDate;
    };

//-- Modules --------------------------------------------------------------------------------------------------------------------

    /**
     *
     * @param year
     * @param month
     * @param day
     * @param hours
     * @param minute
     * @param second
     * @constructor
     */
    var GregorianDate = function (year, month, day, hours, minute, second) {

        var GREGORIAN_EPOCH = 1721425.5;

        /**
         *
         * @param year
         * @param month
         * @param day
         * @return {number} JulianDayNumber
         */
        var toJulianDayNumber = function (year, month, day) {
            return (GREGORIAN_EPOCH - 1) +
                (365 * (year - 1)) +
                Math.floor((year - 1) / 4) +
                (-Math.floor((year - 1) / 100)) +
                Math.floor((year - 1) / 400) +
                Math.floor((((367 * month) - 362) / 12) + ((month <= 2) ? 0 : (isLeap(year) ? -1 : -2)) + day);
        };

        /**
         *
         * @return {number[]} [year, month, day]
         */
        var toGregorian = function () {
            var jd = getJulianDayNumber();

            var wjd = Math.floor(jd - 0.5) + 0.5;
            var depoch = wjd - GREGORIAN_EPOCH;
            var quadricent = Math.floor(depoch / 146097);
            var dqc = mod(depoch, 146097);
            var cent = Math.floor(dqc / 36524);
            var dcent = mod(dqc, 36524);
            var quad = Math.floor(dcent / 1461);
            var dquad = mod(dcent, 1461);
            var yindex = Math.floor(dquad / 365);
            var year = (quadricent * 400) + (cent * 100) + (quad * 4) + yindex;
            if (!((cent == 4) || (yindex == 4))) {
                year++;
            }
            var yearday = wjd - toJulianDayNumber(year, 1, 1);
            var leapadj = ((wjd < toJulianDayNumber(year, 3, 1)) ? 0 : (isLeap(year) ? 1 : 2));
            var month = Math.floor((((yearday + leapadj) * 12) + 373) / 367);
            var day = (wjd - toJulianDayNumber(year, month, 1)) + 1;

            return [year, month, day];
        };

        /**
         *
         * @param year
         * @return {boolean}
         */
        var isLeap = function (year) {
            return ((year % 4) == 0) && (!(((year % 100) == 0) && ((year % 400) != 0)));
        };

        /**
         *
         * @param year
         * @param month
         * @return {number}
         */
        var daysOfMonth = function (year, month) {
            var countDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
            if (month < 1 || month > 12) {
                throw new RangeException("$month Out Of Range Exception");
            }
            if (year && isLeap(year) && month == 2) {
                return 29;
            }
            return countDays[month - 1];
        };

        /**
         *
         * @param year
         * @param month
         * @return {number} DayOfWeek
         */
        var startDayOfMonth = function (year, month) {
            return dayOfWeek(toJulianDayNumber(year, month, 1));
        };

        /**
         *
         * @param year
         * @param month
         * @return {number} DayOfWeek
         */
        var lastDayOfMonth = function (year, month) {
            return dayOfWeek(toJulianDayNumber(year, month, daysOfMonth(year, month)));
        };

        /**
         *
         * @param month
         * @return {string | string[]}
         */
        var monthName = function (month, short) {
            var names = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            if (month === undefined) {
                return names;
            }
            if (month < 1 || month > 12) {
                throw new RangeException("$month Out Of Range Exception");
            }
            if (short) {
                return names[month - 1].substr(0, 3);
            }
            return names[month - 1];
        };

        /**
         *
         * @param dayOfWeek
         * @param short
         * @return {string}
         */
        var nameOfDay = function (dayOfWeek, short) {
            var names = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
            if (dayOfWeek < 0 || dayOfWeek > 6) {
                throw new RangeException("$dayOfWeek Out Of Range Exception");
            }
            if (short) {
                return names[dayOfWeek].substr(0, 3);
            }
            return names[dayOfWeek];
        };

        /**
         *
         * @return {DateTime}
         */
        var get = function () {
            var datetime = new DateTime();
            var utcTimestamp = getTimestamp();
            var utcDate = toGregorian();
            var localTimestamp = utcTimestamp + getOffset();
            var localDate;
            
            datetime.timestamp(utcTimestamp);
            datetime.julianDay(getJulianDayNumber());
           
            datetime.utcYear(utcDate[0]);
            datetime.utcMonth(utcDate[1]);
            datetime.utcDay(utcDate[2]);
            datetime.utcHour(_hour);
            datetime.utcMinute(_minute);
            datetime.utcSecond(_second);
            datetime.utcDayName(false, nameOfDay(dayOfWeek(getJulianDayNumber())));
            datetime.utcDayName(true, nameOfDay(dayOfWeek(getJulianDayNumber())));
            datetime.utcMonthName(false, monthName(utcDate[1]));
            datetime.utcMonthName(true, monthName(utcDate[1], true));
            
            setTimestamp(localTimestamp);
            localDate = toGregorian();
            
            datetime.year(localDate[0]);
            datetime.month(localDate[1]);
            datetime.day(localDate[2]);
            datetime.hour(_hour);
            datetime.minute(_minute);
            datetime.second(_second);
            datetime.dayName(false, nameOfDay(dayOfWeek(getJulianDayNumber())));
            datetime.dayName(true, nameOfDay(dayOfWeek(getJulianDayNumber())));
            datetime.monthName(false, monthName(localDate[1]));
            datetime.monthName(true, monthName(localDate[1], true));

            setTimestamp(utcTimestamp);

            return datetime;
        };

        //construct
        {
            if (year && month && day) {
                var date = new Date();
                _hour = hours !== undefined ? hours : date.getHours();
                _minute = minute !== undefined ? minute : date.getMinutes();
                _second = second !== undefined ? second : date.getSeconds();
                _julianDayNumber = toJulianDayNumber(year, month, day);
                setTimestamp(getTimestamp() - getOffset());
            }
        }

        this.daysOfMonth = daysOfMonth;
        this.startDayOfMonth = startDayOfMonth;
        this.lastDayOfMonth = lastDayOfMonth;
        this.monthName = monthName;
        this.nameOfDay = nameOfDay;
        this.get = get;
    };

    /**
     *
     * @param year
     * @param month
     * @param day
     * @param hours
     * @param minute
     * @param second
     * @constructor
     */
    var IslamicDate = function (year, month, day, hours, minute, second) {
        var ISLAMIC_EPOCH = 1948439.5;

        /**
         *
         * @param year
         * @param month
         * @param day
         * @return {number}
         */
        var toJulianDayNumber = function (year, month, day) {
            return (day +
                Math.ceil(29.5 * (month - 1)) +
                (year - 1) * 354 +
                Math.floor((3 + (11 * year)) / 30) +
                ISLAMIC_EPOCH) - 1;
        };

        /**
         *
         * @return {number[]} [year, month, day]
         */
        var toIslamic = function () {
            var jd = getJulianDayNumber();
            jd = Math.floor(jd) + 0.5;
            var year = Math.floor(((30 * (jd - ISLAMIC_EPOCH)) + 10646) / 10631);
            var month = Math.min(12, Math.ceil((jd - (29 + toJulianDayNumber(year, 1, 1))) / 29.5) + 1);
            var day = (jd - toJulianDayNumber(year, month, 1)) + 1;
            return [year, month, day];
        };

        /**
         *
         * @param year
         * @return {boolean}
         */
        var isLeap = function (year) {
            return (((year * 11) + 14) % 30) < 11;
        };

        /**
         *
         * @param month
         * @return {number}
         */
        var daysOfMonth = function (year, month) {
            var countDays = [30, 29, 30, 29, 30, 29, 30, 29, 30, 29, 30, 29]; //30
            if (month < 1 || month > 12) {
                throw new RangeException("$month Out Of Range Exception");
            }
            if (year && isLeap(year) && month == 12) {
                return 30;
            }
            return countDays[month - 1];
        };

        /**
         *
         * @param year
         * @param month
         * @return {number} DayOfWeek
         */
        var startDayOfMonth = function (year, month) {
            return dayOfWeek(toJulianDayNumber(year, month, 1));
        };

        /**
         *
         * @param year
         * @param month
         * @return {number} DayOfWeek
         */
        var lastDayOfMonth = function (year, month) {
            return dayOfWeek(toJulianDayNumber(year, month, daysOfMonth(year, month)));
        };

        /**
         *
         * @param month
         * @return {string | string[]}
         */
        var monthName = function (month) {
            var names = ["محرم", "صفر", "ربیع الاول", "ربیع الثانی", "جمادی الاول", "جمادی الثانی", "رجب", "شعبان", "رمضان", "شوال", "ذی‌القعده", "ذی‌الحجه"];
            if (month === undefined) {
                return names;
            }
            if (month < 1 || month > 12) {
                throw new RangeException("$month Out Of Range Exception");
            }
            return names[month - 1];
        };

        /**
         *
         * @param dayOfWeek
         * @param short
         * @return {string}
         */
        var nameOfDay = function (dayOfWeek, short) {
            var names = ["الأحد", "الاثنین", "الثلاثاء", "الأربعاء", "الخمیس", "الجمعة", "السبت"];
            var shortNames = ["ح", "ن", "ث", "ر", "خ", "ج", "س"];
            if (dayOfWeek < 0 || dayOfWeek > 6) {
                throw new RangeException("$dayOfWeek Out Of Range Exception");
            }
            if (short) {
                return shortNames[dayOfWeek];
            }
            return names[dayOfWeek];
        };

        /**
         *
         * @return {DateTime}
         */
        var get = function () {
            var datetime = new DateTime();
            var utcTimestamp = getTimestamp();
            var utcDate = toIslamic();
            var localTimestamp = utcTimestamp + getOffset();
            var localDate;
            
            datetime.timestamp(utcTimestamp);
            datetime.julianDay(getJulianDayNumber());
           
            datetime.utcYear(utcDate[0]);
            datetime.utcMonth(utcDate[1]);
            datetime.utcDay(utcDate[2]);
            datetime.utcHour(_hour);
            datetime.utcMinute(_minute);
            datetime.utcSecond(_second);
            datetime.utcDayName(false, nameOfDay(dayOfWeek(getJulianDayNumber())));
            datetime.utcDayName(true, nameOfDay(dayOfWeek(getJulianDayNumber())));
            datetime.utcMonthName(false, monthName(utcDate[1]));
            datetime.utcMonthName(true, monthName(utcDate[1], true));
            
            setTimestamp(localTimestamp);
            localDate = toIslamic();
            
            datetime.year(localDate[0]);
            datetime.month(localDate[1]);
            datetime.day(localDate[2]);
            datetime.hour(_hour);
            datetime.minute(_minute);
            datetime.second(_second);
            datetime.dayName(false, nameOfDay(dayOfWeek(getJulianDayNumber())));
            datetime.dayName(true, nameOfDay(dayOfWeek(getJulianDayNumber())));
            datetime.monthName(false, monthName(localDate[1]));
            datetime.monthName(true, monthName(localDate[1], true));

            setTimestamp(utcTimestamp);

            return datetime;
        };

        //construct
        {
            if (year && month && day) {
                var date = new Date();
                _hour = hours !== undefined ? hours : date.getHours();
                _minute = minute !== undefined ? minute : date.getMinutes();
                _second = second !== undefined ? second : date.getSeconds();
                _julianDayNumber = toJulianDayNumber(year, month, day);
                setTimestamp(getTimestamp() - getOffset());
            }
        }

        this.daysOfMonth = daysOfMonth;
        this.startDayOfMonth = startDayOfMonth;
        this.lastDayOfMonth = lastDayOfMonth;
        this.monthName = monthName;
        this.nameOfDay = nameOfDay;
        this.get = get;
    };

    /**
     *
     * @param year
     * @param month
     * @param day
     * @param hours
     * @param minute
     * @param second
     * @constructor
     */
    var ShiaDate = function (year, month, day, hours, minute, second) {
        var ISLAMIC_EPOCH = 1948439.5;

        /**
         *
         * @param year
         * @param month
         * @param day
         * @return {number}
         */
        var toJulianDayNumber = function (year, month, day) {
            var days = 0;
            for (var i = 0; i < (month - 1); i++) {
                days += daysOfMonth(year, i + 1);
            }
            return (day +
                days +
                (year - 1) * 354 +
                Math.floor((3 + (11 * year)) / 30) +
                ISLAMIC_EPOCH) - 1;
        };

        var toShia = function () {
            var jd = getJulianDayNumber();
            jd = Math.floor(jd) + 0.5;
            var year = Math.floor(((30 * (jd - ISLAMIC_EPOCH)) + 10646) / 10631);
            var month = Math.min(12, Math.ceil((jd - (29 + toJulianDayNumber(year, 1, 1))) / 29.5) + 1);
            var dayOfYear = jd - toJulianDayNumber(year, 1, 1);
            var days = 0;
            for (var i = 0; i < 12; i++) {
                days += daysOfMonth(year, i + 1);
                if (dayOfYear < days) {
                    month = i + 1;
                    break;
                }
            }
            var day = (jd - ((days - daysOfMonth(year, month)) + (year - 1) * 354 + Math.floor((3 + (11 * year)) / 30) + ISLAMIC_EPOCH) + 1);
            return [year, month, day];
        };

        /**
         *
         * @param year
         * @return {boolean}
         */
        var isLeap = function (year) {
            return (((year * 11) + 14) % 30) < 11;
        };

        /**
         *
         * @param year
         * @param month
         * @return {number}
         */
        var daysOfMonth = function (year, month) {
            var islamicCountDays = [30, 29, 30, 29, 30, 29, 30, 29, 30, 29, 30, 29]; //30
            var countDays = {
                1435: [29, 30, 29, 30, 29, 30, 29, 30, 30, 30, 29, 30],
                1436: [29, 30, 29, 29, 30, 29, 30, 29, 30, 29, 30, 30],
                1437: [29, 30, 30, 29, 30, 29, 29, 30, 29, 29, 30, 30],
                1438: [29, 30, 30, 30, 29, 30, 29, 29, 30, 29, 29, 30],
                1439: [29, 30, 30, 30, 30, 29, 30, 29, 29, 30, 29, 29],
                1440: [30, 29, 30, 30, 30, 29, 29, 30, 29, 30, 29, 29],
            };

            if (month < 1 || month > 12) {
                throw new RangeException("$month Out Of Range Exception");
            }

            if (countDays[year] === undefined) {
                return islamicCountDays[month - 1];
            }

            return countDays[year][month - 1];
        };

        /**
         *
         * @param year
         * @param month
         * @return {number} DayOfWeek
         */
        var startDayOfMonth = function (year, month) {
            return dayOfWeek(toJulianDayNumber(year, month, 1));
        };

        /**
         *
         * @param year
         * @param month
         * @return {number} DayOfWeek
         */
        var lastDayOfMonth = function (year, month) {
            return dayOfWeek(toJulianDayNumber(year, month, daysOfMonth(year, month)));
        };

        /**
         *
         * @param month
         * @return {string | string[]}
         */
        var monthName = function (month) {
            var names = ["محرم", "صفر", "ربیع الاول", "ربیع الثانی", "جمادی الاول", "جمادی الثانی", "رجب", "شعبان", "رمضان", "شوال", "ذی‌القعده", "ذی‌الحجه"];
            if (month === undefined) {
                return names;
            }
            if (month < 1 || month > 12) {
                throw new RangeException("$month Out Of Range Exception");
            }
            return names[month - 1];
        };

        /**
         *
         * @param dayOfWeek
         * @param short
         * @return {string}
         */
        var nameOfDay = function (dayOfWeek, short) {
            var names = ["الأحد", "الاثنین", "الثلاثاء", "الأربعاء", "الخمیس", "الجمعة", "السبت"];
            var shortNames = ["ح", "ن", "ث", "ر", "خ", "ج", "س"];
            if (dayOfWeek < 0 || dayOfWeek > 6) {
                throw new RangeException("$dayOfWeek Out Of Range Exception");
            }
            if (short) {
                return shortNames[dayOfWeek];
            }
            return names[dayOfWeek];
        };

        /**
         *
         * @return {DateTime}
         */
        var get = function () {
            var datetime = new DateTime();
            var utcTimestamp = getTimestamp();
            var utcDate = toShia();
            var localTimestamp = utcTimestamp + getOffset();
            var localDate;
            
            datetime.timestamp(utcTimestamp);
            datetime.julianDay(getJulianDayNumber());
           
            datetime.utcYear(utcDate[0]);
            datetime.utcMonth(utcDate[1]);
            datetime.utcDay(utcDate[2]);
            datetime.utcHour(_hour);
            datetime.utcMinute(_minute);
            datetime.utcSecond(_second);
            datetime.utcDayName(false, nameOfDay(dayOfWeek(getJulianDayNumber())));
            datetime.utcDayName(true, nameOfDay(dayOfWeek(getJulianDayNumber())));
            datetime.utcMonthName(false, monthName(utcDate[1]));
            datetime.utcMonthName(true, monthName(utcDate[1], true));
            
            setTimestamp(localTimestamp);
            localDate = toShia();
            
            datetime.year(localDate[0]);
            datetime.month(localDate[1]);
            datetime.day(localDate[2]);
            datetime.hour(_hour);
            datetime.minute(_minute);
            datetime.second(_second);
            datetime.dayName(false, nameOfDay(dayOfWeek(getJulianDayNumber())));
            datetime.dayName(true, nameOfDay(dayOfWeek(getJulianDayNumber())));
            datetime.monthName(false, monthName(localDate[1]));
            datetime.monthName(true, monthName(localDate[1], true));

            setTimestamp(utcTimestamp);

            return datetime;
        };

        //construct
        {
            if (year && month && day) {
                var date = new Date();
                _hour = hours !== undefined ? hours : date.getHours();
                _minute = minute !== undefined ? minute : date.getMinutes();
                _second = second !== undefined ? second : date.getSeconds();
                _julianDayNumber = toJulianDayNumber(year, month, day);
                setTimestamp(getTimestamp() - getOffset());
            }
        }

        this.daysOfMonth = daysOfMonth;
        this.startDayOfMonth = startDayOfMonth;
        this.lastDayOfMonth = lastDayOfMonth;
        this.monthName = monthName;
        this.nameOfDay = nameOfDay;
        this.get = get;
    };

    /**
     *
     * @param year
     * @param month
     * @param day
     * @param hours
     * @param minute
     * @param second
     * @constructor
     */
    var JalaliDate = function (year, month, day, hours, minute, second) {
        var PERSIAN_EPOCH = 1948320.5;

        /**
         *
         * @param year
         * @param month
         * @param day
         * @returns {Number}
         */
        var toJulianDayNumber = function (year, month, day) {
            var epbase = year - ((year >= 0) ? 474 : 473);
            var epyear = 474 + mod(epbase, 2820);

            return day +
                ((month <= 7) ? ((month - 1) * 31) : (((month - 1) * 30) + 6)) +
                Math.floor(((epyear * 682) - 110) / 2816) +
                (epyear - 1) * 365 +
                Math.floor(epbase / 2820) * 1029983 +
                (PERSIAN_EPOCH - 1);
        };

        /**
         *
         * @returns {number[]} [year, month, day]
         */
        var toJalali = function () {
            var jd = getJulianDayNumber();
            var year, month, day, depoch, cycle, cyear, ycycle, aux1, aux2, yday;

            jd = Math.floor(jd) + 0.5;

            depoch = jd - toJulianDayNumber(475, 1, 1);
            cycle = Math.floor(depoch / 1029983);
            cyear = mod(depoch, 1029983);
            if (cyear == 1029982) {
                ycycle = 2820;
            } else {
                aux1 = Math.floor(cyear / 366);
                aux2 = mod(cyear, 366);
                ycycle = Math.floor(((2134 * aux1) + (2816 * aux2) + 2815) / 1028522) +
                    aux1 + 1;
            }
            year = ycycle + (2820 * cycle) + 474;
            if (year <= 0) {
                year--;
            }
            yday = (jd - toJulianDayNumber(year, 1, 1)) + 1;
            month = (yday <= 186) ? Math.ceil(yday / 31) : Math.ceil((yday - 6) / 30);
            day = (jd - toJulianDayNumber(year, month, 1)) + 1;
            return [year, month, day];
        };

        /**
         *
         * @param year
         * @returns {boolean}
         */
        var isLeap = function (year) {
            return ((((((year - ((year > 0) ? 474 : 473)) % 2820) + 474) + 38) * 682) % 2816) < 682;
        };

        /**
         * @param year
         * @param month
         * @returns {number}
         */
        var daysOfMonth = function (year, month) {
            var countDays = [31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29]; //30
            if (month < 1 || month > 12) {
                throw new RangeException("$month Out Of Range Exception");
            }
            if (year && isLeap(year) && month == 12) {
                return 30;
            }
            return countDays[month - 1];
        };

        /**
         *
         * @param year
         * @param month
         * @return {number} DayOfWeek
         */
        var startDayOfMonth = function (year, month) {
            return dayOfWeek(toJulianDayNumber(year, month, 1));
        };

        /**
         *
         * @param year
         * @param month
         * @return {number} DayOfWeek
         */
        var lastDayOfMonth = function (year, month) {
            return dayOfWeek(toJulianDayNumber(year, month, daysOfMonth(year, month)));
        };

        /**
         *
         * @param month
         * @return {string | string[]}
         */
        var monthName = function (month) {
            var names = ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند'];
            if (month === undefined) {
                return names;
            }
            if (month < 1 || month > 12) {
                throw new RangeException("$month Out Of Range Exception");
            }
            return names[month - 1];
        };

        /**
         *
         * @param dayOfWeek
         * @param short
         * @return {string}
         */
        var nameOfDay = function (dayOfWeek, short) {
            var names = ["یک شنبه", "دوشنبه", "سه شنبه", "چهارشنبه", "پنج شنبه", "جمعه", "شنبه"];
            if (dayOfWeek < 0 || dayOfWeek > 6) {
                throw new RangeException("$dayOfWeek Out Of Range Exception");
            }
            if (short) {
                return names[dayOfWeek].substr(0, 2);
            }
            return names[dayOfWeek];
        };

        /**
         *
         * @returns {DateTime}
         */
        var get = function () {
            var datetime = new DateTime();
            var utcTimestamp = getTimestamp();
            var utcDate = toJalali();
            var localTimestamp = utcTimestamp + getOffset();
            var localDate;
            
            datetime.timestamp(utcTimestamp);
            datetime.julianDay(getJulianDayNumber());
           
            datetime.utcYear(utcDate[0]);
            datetime.utcMonth(utcDate[1]);
            datetime.utcDay(utcDate[2]);
            datetime.utcHour(_hour);
            datetime.utcMinute(_minute);
            datetime.utcSecond(_second);
            datetime.utcDayName(false, nameOfDay(dayOfWeek(getJulianDayNumber())));
            datetime.utcDayName(true, nameOfDay(dayOfWeek(getJulianDayNumber())));
            datetime.utcMonthName(false, monthName(utcDate[1]));
            datetime.utcMonthName(true, monthName(utcDate[1], true));
            
            setTimestamp(localTimestamp);
            localDate = toJalali();
            
            datetime.year(localDate[0]);
            datetime.month(localDate[1]);
            datetime.day(localDate[2]);
            datetime.hour(_hour);
            datetime.minute(_minute);
            datetime.second(_second);
            datetime.dayName(false, nameOfDay(dayOfWeek(getJulianDayNumber())));
            datetime.dayName(true, nameOfDay(dayOfWeek(getJulianDayNumber())));
            datetime.monthName(false, monthName(localDate[1]));
            datetime.monthName(true, monthName(localDate[1], true));

            setTimestamp(utcTimestamp);

            return datetime;
        };

        //construct
        {
            if (year && month && day) {
                var date = new Date();
                _hour = (hours !== undefined) ? hours : date.getHours();
                _minute = (minute !== undefined) ? minute : date.getMinutes();
                _second = (second !== undefined) ? second : date.getSeconds();
                _julianDayNumber = toJulianDayNumber(year, month, day);
                setTimestamp(getTimestamp() - getOffset());
            }
        }

        this.daysOfMonth = daysOfMonth;
        this.startDayOfMonth = startDayOfMonth;
        this.lastDayOfMonth = lastDayOfMonth;
        this.monthName = monthName;
        this.nameOfDay = nameOfDay;
        this.get = get;
    };

    /**
     *
     * @param timestamp
     * @param julianDay
     * @param year
     * @param month
     * @param day
     * @param hour
     * @param minute
     * @param second
     * @param dayName
     * @param shortDayName
     * @param monthName
     * @param shortMonthName
     * @constructor
     */
    var DateTime = function () {
        var _timestamp;
        var _offset;
        var _julianDay;
        var _year;
        var _month;
        var _day;
        var _hour;
        var _minute;
        var _second;
        var _dayName;
        var _shortDayName;
        var _monthName;
        var _shortMonthName;
        var _utcYear;
        var _utcMonth;
        var _utcDay;
        var _utcHour;
        var _utcMinute;
        var _utcSecond;
        var _utcDayName;
        var _utcShortDayName;
        var _utcMonthName;
        var _utcShortMonthName;


        var leadingZeros = function (number, length) {
            return String(number).length < length ? leadingZeros("0" + number, length) : number;
        };

        this.timestamp = function (timestamp) {
            if (timestamp !== undefined) {
                _timestamp = Number(timestamp) || 0;
                return;
            } 
            return _timestamp;
        };

        this.julianDay = function (julianDay) {
            if (julianDay !== undefined) {
                _julianDay = Number(julianDay) || 0;
                return;
            }
            return _julianDay;
        };

        this.year = function (year) {
            if (year !== undefined) {
                _year = Number(year) || 0;
                return;
            }
            return _year;
        };

        this.month = function (month) {
            if (month !== undefined) {
                _month = Number(month) || 0;
                return;
            }
            return _month;
        };

        this.day = function (day) {
            if (day !== undefined) {
                _day = Number(day) || 0;
                return;
            }
            return _day;
        };

        this.hour = function (hour) {
            if (hour !== undefined) {
                _hour = Number(hour) || 0;
                return;
            }
            return _hour;
        };

        this.minute = function (minute) {
            if (minute !== undefined) {
                _minute = Number(minute) || 0;
                return;
            }
            return _minute;
        };

        this.second = function (second) {
            if (second !== undefined) {
                _second = Number(second) || 0;
                return;
            }
            return _second;
        };

        this.dayName = function (short, dayName) {
            if (dayName) {
                if (short)
                    _shortDayName = dayName;
                else
                    _dayName = dayName;

                return;
            }

            if (short) 
                return _shortDayName;
            return _dayName;
        };

        this.monthName = function (short, monthName) {
            if (monthName) {
                if (short)
                    _shortMonthName = monthName;
                else
                    _monthName = monthName;

                return;
            }

            if (short)
                return _shortMonthName;
            return _monthName;
        };

        this.utcYear = function (year) {
            if (year !== undefined) {
                _utcYear = Number(year) || 0;
                return;
            }
            return _utcYear;
        };

        this.utcMonth = function (month) {
            if (month !== undefined) {
                _utcMonth = Number(month) || 0;
                return;
            }
            return _utcMonth;
        };

        this.utcDay = function (day) {
            if (day !== undefined) {
                _utcDay = Number(day) || 0;
                return;
            }
            return _utcDay;
        };

        this.utcHour = function (hour) {
            if (hour !== undefined) {
                _utcHour = Number(hour) || 0;
                return;
            }
            return _utcHour;
        };

        this.utcMinute = function (minute) {
            if (minute !== undefined) {
                _utcMinute = Number(minute) || 0;
                return;
            }
            return _utcMinute;
        };

        this.utcSecond = function (second) {
            if (second !== undefined) {
                _utcSecond = Number(second) || 0;
                return;
            }
            return _utcSecond;
        };

        this.utcDayName = function (short, dayName) {
            if (dayName) {
                if (short)
                    _utcShortDayName = dayName;
                else
                    _utcDayName = dayName;

                return;
            }

            if (short) 
                return _utcShortDayName;
            return _utcDayName;
        };

        this.utcMonthName = function (short, monthName) {
            if (monthName) {
                if (short)
                    _utcShortMonthName = monthName;
                else
                    _utcMonthName = monthName;

                return;
            }

            if (short)
                return _utcShortMonthName;
            return _utcMonthName;
        };

        this.toString = function () {
            return _year + "-" + _month + "-" + _day + " " + _hour + ":" + _minute + ":" + _second;
        };

        this.format = function (pattern) {
            var temp = pattern;
            temp = temp.replace(/%Day%/g, leadingZeros(_day, 2));
            temp = temp.replace(/%day%/g, _day);
            temp = temp.replace(/%day:name:short%|%Day:name:short%/g, _shortDayName);
            temp = temp.replace(/%day:name%|%Day:name%/g, _dayName);
            temp = temp.replace(/%month:name%|%Month:name%/g, _monthName);
            temp = temp.replace(/%month:name:short%|%Month:name:short%/g, _shortMonthName);
            temp = temp.replace(/%Month%/g, leadingZeros(_month, 2));
            temp = temp.replace(/%month%/g, _month);
            temp = temp.replace(/%Year%/g, _year);
            temp = temp.replace(/%year%/g, String(_year).substr(2));
            temp = temp.replace(/%meridiem%/g, (_hour > 12 ? 'pm' : 'am'));
            temp = temp.replace(/%Meridiem%/g, (_hour > 12 ? 'PM' : 'pm'));
            temp = temp.replace(/%hour:12%/g, (_hour > 12 ? _hour - 12 : _hour));
            temp = temp.replace(/%hour%/g, _hour);
            temp = temp.replace(/%Hour:12%/g, leadingZeros((_hour > 12 ? _hour - 12 : _hour), 2));
            temp = temp.replace(/%Hour%/g, leadingZeros(_hour, 2));
            temp = temp.replace(/%minute%/g, _minute);
            temp = temp.replace(/%Minute%/g, leadingZeros(_minute, 2));
            temp = temp.replace(/%second%/g, _second);
            temp = temp.replace(/%Second%/g, leadingZeros(_second, 2));

            temp = temp.replace(/%UDay%/g, leadingZeros(_utcDay, 2));
            temp = temp.replace(/%uday%/g, _utcDay);
            temp = temp.replace(/%uday:name:short%|%UDay:name:short%/g, _utcShortDayName);
            temp = temp.replace(/%uday:name%|%UDay:name%/g, _utcDayName);
            temp = temp.replace(/%umonth:name%|%UMonth:name%/g, _utcMonthName);
            temp = temp.replace(/%umonth:name:short%|%UMonth:name:short%/g, _utcShortMonthName);
            temp = temp.replace(/%UMonth%/g, leadingZeros(_utcMonth, 2));
            temp = temp.replace(/%umonth%/g, _utcMonth);
            temp = temp.replace(/%UYear%/g, _utcYear);
            temp = temp.replace(/%uyear%/g, String(_utcYear).substr(2));
            temp = temp.replace(/%umeridiem%/g, (_utcHour > 12 ? 'pm' : 'am'));
            temp = temp.replace(/%UMeridiem%/g, (_utcHour > 12 ? 'PM' : 'AM'));
            temp = temp.replace(/%uhour:12%/g, (_utcHour > 12 ? _utcHour - 12 : _utcHour));
            temp = temp.replace(/%uhour%/g, _utcHour);
            temp = temp.replace(/%UHour:12%/g, leadingZeros((_utcHour > 12 ? _utcHour - 12 : _utcHour), 2));
            temp = temp.replace(/%UHour%/g, leadingZeros(_utcHour, 2));
            temp = temp.replace(/%uminute%/g, _utcMinute);
            temp = temp.replace(/%UMinute%/g, leadingZeros(_utcMinute, 2));
            temp = temp.replace(/%usecond%/g, _utcSecond);
            temp = temp.replace(/%USecond%/g, leadingZeros(_utcSecond, 2));

            return temp;
        };
    };

//-- Public --------------------------------------------------------------------------------------------------------------------

    this.gregorian = gregorian;
    this.islamic = islamic;
    this.shia = shia;
    this.jalali = jalali;
    this.setTimestamp = setTimestamp;
    this.getTimestamp = getTimestamp;
    this.setJulianDayNumber = setJulianDayNumber;
    this.getJulianDayNumber = getJulianDayNumber;
    this.setOffset = setOffset;
    this.getOffset = getOffset;
    this.dayOfWeek = dayOfWeek;

//construct
    {
        var date = new Date();
        var time = (date.getTime() / 1000);
        
        timestamp = timestamp || 'now';
        offset = offset || date.getTimezoneOffset() * 60;

    
        if(!Number.isNaN(Number(timestamp))) {
            time = Number.parseInt(timestamp);
        }

        setOffset(offset);
        setTimestamp(time);
    }
};