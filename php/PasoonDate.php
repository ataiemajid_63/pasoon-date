<?php

namespace Pasoon;

class PasoonDate
{
    private $J1970 = 2440587.5;                // Julian date at Unix epoch: 1970-01-01
    private $jalaliDate;
    private $gregorianDate;
    private $islamicDate;
    private $shiaDate;

    private $julianDayNumber;
    private $offset;
    private $hours;
    private $minute;
    private $second;

    /**
     * PasoonDate constructor.
     * @param int|string $timestamp
     * @param int $offset
     */
    public function __construct($timestamp = 'now', $offset = null)
    {
        $time = time();

        if (is_numeric($timestamp)) {
            $time = (integer) $timestamp;
        }

        $this->setOffset($offset ?: date('Z'));
        $this->setTimestamp($time);
    }

    private function julianTimeToTime($julianNumber)
    {
        $julianNumber += 0.5; /* Astronomical to civil */
        $ij = (($julianNumber - floor($julianNumber)) * 86400.0) + 0.5;
        return [
            floor($ij / 3600),
            floor(($ij / 60) % 60),
            floor($ij % 60)
        ];
    }

    public function setTimestamp($timestamp)
    {
        $date = floor($timestamp / (60 * 60 * 24));
        $time = $timestamp - ($date * 60 * 60 * 24);
        $hour = floor($time / (60*60));
        $minute = floor(($time / 60) - ($hour * 60));
        $second = $time - ((($hour * 60) + $minute) * 60);

        $this->setJulianDayNumber(($this->J1970 + $date));
        $this->setHours($hour);
        $this->setMinute($minute);
        $this->setSecond($second);
    }

    public function getTimestamp()
    {
        $timestamp = ($this->getJulianDayNumber() - $this->J1970) * (60 * 60 * 24);
        return (($this->getHours() * 60 * 60) + ($this->getMinute() * 60) + $this->getSecond()) + $timestamp;
    }

    public function setJulianDayNumber($number)
    {
        $this->julianDayNumber = $number;
    }

    public function getJulianDayNumber()
    {
        return $this->julianDayNumber;
    }

    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @return mixed
     */
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * @param mixed $hours
     */
    public function setHours($hours)
    {
        $this->hours = (int)$hours;
    }

    /**
     * @return mixed
     */
    public function getMinute()
    {
        return $this->minute;
    }

    /**
     * @param mixed $minute
     */
    public function setMinute($minute)
    {
        $this->minute = (int)$minute;
    }

    /**
     * @return mixed
     */
    public function getSecond()
    {
        return $this->second;
    }

    /**
     * @param mixed $second
     */
    public function setSecond($second)
    {
        $this->second = (int)$second;
    }


    /**
     * @param bool|int $year
     * @param bool|int $month
     * @param bool|int $day
     * @param bool|int $hours
     * @param bool|int $minute
     * @param bool|int $second
     * @return JalaliDate
     */
    public function jalali($year = false, $month = false, $day = false, $hours = false, $minute = false, $second = false)
    {
        if (empty($this->jalaliDate) or $year)
            $this->jalaliDate = new JalaliDate($this, $year, $month, $day, $hours, $minute, $second);
        return $this->jalaliDate;
    }

    /**
     * @param bool|int $year
     * @param bool|int $month
     * @param bool|int $day
     * @param bool|int $hours
     * @param bool|int $minute
     * @param bool|int $second
     * @return GregorianDate
     */
    public function gregorian($year = false, $month = false, $day = false, $hours = false, $minute = false, $second = false)
    {
        if (empty($this->gregorianDate) or $year) {
            $this->gregorianDate = new GregorianDate($this, $year, $month, $day, $hours, $minute, $second);
        }
        return $this->gregorianDate;
    }

    /**
     * @param bool|int $year
     * @param bool|int $month
     * @param bool|int $day
     * @param bool|int $hours
     * @param bool|int $minute
     * @param bool|int $second
     * @return IslamicDate
     */
    public function islamic($year = false, $month = false, $day = false, $hours = false, $minute = false, $second = false)
    {
        if (empty($this->islamicDate) or $year)
            $this->islamicDate = new IslamicDate($this, $year, $month, $day, $hours, $minute, $second);
        return $this->islamicDate;
    }

    /**
     * @param bool|int $year
     * @param bool|int $month
     * @param bool|int $day
     * @param bool|int $hours
     * @param bool|int $minute
     * @param bool|int $second
     * @return ShiaDate
     */
    public function shia($year = false, $month = false, $day = false, $hours = false, $minute = false, $second = false)
    {
        if (empty($this->shiaDate) or $year)
            $this->shiaDate = new ShiaDate($this, $year, $month, $day, $hours, $minute, $second);
        return $this->shiaDate;
    }
}

class AbstractPasoonDate
{
    protected function mod($a, $b)
    {
        return $a - ($b * floor($a / $b));
    }

    protected function dayOfWeek($julianDayNumber)
    {
        return $this->mod(floor(($julianDayNumber + 1.5)), 7);
    }
}

class GregorianDate extends AbstractPasoonDate
{
    private $GREGORIAN_EPOCH = 1721425.5;
    private $pasoonDate;

    /**
     * @param PasoonDate $pasoonDate
     * @param $year
     * @param $month
     * @param $day
     * @param $hours
     * @param $minute
     * @param $second
     */
    public function __construct($pasoonDate, $year, $month, $day, $hours, $minute, $second)
    {
        $this->pasoonDate = $pasoonDate;
        if ($year and $month and $day) {
            $pasoonDate->setHours(isset($hours) ? $hours : date('H'));
            $pasoonDate->setMinute(isset($minute) ? $minute : date('i'));
            $pasoonDate->setSecond(isset($second) ? $second : date('s'));
            $pasoonDate->setJulianDayNumber($this->toJulianDayNumber($year, $month, $day));
            $pasoonDate->setTimestamp($pasoonDate->getTimestamp() - $pasoonDate->getOffset());
        }
    }


    private function toJulianDayNumber($year, $month, $day)
    {
        return ($this->GREGORIAN_EPOCH - 1) +
        (365 * ($year - 1)) +
        floor(($year - 1) / 4) +
        (-floor(($year - 1) / 100)) +
        floor(($year - 1) / 400) +
        floor((((367 * $month) - 362) / 12) + (($month <= 2) ? 0 : ($this->isLeap($year) ? -1 : -2)) + $day);
    }

    private function toGregorian()
    {
        $jd = $this->pasoonDate->getJulianDayNumber();

        $wjd = floor($jd - 0.5) + 0.5;
        $depoch = $wjd - $this->GREGORIAN_EPOCH;
        $quadricent = floor($depoch / 146097);
        $dqc = $this->mod($depoch, 146097);
        $cent = floor($dqc / 36524);
        $dcent = $this->mod($dqc, 36524);
        $quad = floor($dcent / 1461);
        $dquad = $this->mod($dcent, 1461);
        $yindex = floor($dquad / 365);
        $year = ($quadricent * 400) + ($cent * 100) + ($quad * 4) + $yindex;
        if (!(($cent == 4) || ($yindex == 4))) {
            $year++;
        }
        $yearday = $wjd - $this->toJulianDayNumber($year, 1, 1);
        $leapadj = (($wjd < $this->toJulianDayNumber($year, 3, 1)) ? 0 : ($this->isLeap($year) ? 1 : 2));
        $month = floor(((($yearday + $leapadj) * 12) + 373) / 367);
        $day = ($wjd - $this->toJulianDayNumber($year, $month, 1)) + 1;

        return [$year, $month, $day];
    }

    private function isLeap($year)
    {
        return (($year % 4) == 0) && (!((($year % 100) == 0) && (($year % 400) != 0)));
    }


    public function daysOfMonth($year, $month)
    {
        $countDays = array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
        if ($month < 1 || $month > 12) {
            throw new Exception("$month Out Of Range Exception");
        }
        if ($year && $this->isLeap($year) && $month == 2) {
            return 29;
        }
        return $countDays[$month - 1];
    }

    public function startDayOfMonth($year, $month)
    {
        return $this->dayOfWeek($this->toJulianDayNumber($year, $month, 1));
    }

    public function lastDayOfMonth($year, $month)
    {
        return $this->dayOfWeek($this->toJulianDayNumber($year, $month, $this->daysOfMonth($year, $month)));
    }

    public function monthName($month, $short = false)
    {
        $names = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        if (empty($month)) {
            return $names;
        }
        if ($month < 1 || $month > 12) {
            throw new Exception("$month Out Of Range Exception");
        }
        if ($short) {
            return substr($names[$month - 1], 0, 3);
        }

        return $names[$month - 1];
    }

    public function nameOfDay($dayOfWeek, $short = false)
    {
        $names = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        if ($dayOfWeek < 0 || $dayOfWeek > 6) {
            throw new Exception("$dayOfWeek Out Of Range Exception");
        }
        if ($short) {
            return substr($names[$dayOfWeek], 0, 3);
        }
        return $names[$dayOfWeek];
    }

    public function get()
    {
        $date = $this->toGregorian();
        $datetime = new PasoonDateTime();
        $utcTimestamp = $this->pasoonDate->getTimestamp();
        $utcDate = $this->toGregorian();
        $localTimestamp = $utcTimestamp + $this->pasoonDate->getOffset();
        $localDate;

        $datetime->setTimestamp($utcTimestamp);
        $datetime->setJulianDay($this->pasoonDate->getJulianDayNumber());

        $datetime->setUTCYear($utcDate[0]);
        $datetime->setUTCMonth($utcDate[1]);
        $datetime->setUTCDay($utcDate[2]);
        $datetime->setUTCHour($this->pasoonDate->getHours());
        $datetime->setUTCMinute($this->pasoonDate->getMinute());
        $datetime->setUTCSecond($this->pasoonDate->getSecond());
        $datetime->setUTCDayName($this->nameOfDay($this->dayOfWeek($this->pasoonDate->getJulianDayNumber())), false);
        $datetime->setUTCDayName($this->nameOfDay($this->dayOfWeek($this->pasoonDate->getJulianDayNumber())), true);
        $datetime->setUTCMonthName($this->monthName($utcDate[1]), false);
        $datetime->setUTCMonthName($this->monthName($utcDate[1], true), true);

        $this->pasoonDate->setTimestamp($localTimestamp);
        $localDate = $this->toGregorian();

        $datetime->setTimestamp($localTimestamp);
        $datetime->setJulianDay($this->pasoonDate->getJulianDayNumber());

        $datetime->setYear($localDate[0]);
        $datetime->setMonth($localDate[1]);
        $datetime->setDay($localDate[2]);
        $datetime->setHour($this->pasoonDate->getHours());
        $datetime->setMinute($this->pasoonDate->getMinute());
        $datetime->setSecond($this->pasoonDate->getSecond());
        $datetime->setDayName($this->nameOfDay($this->dayOfWeek($this->pasoonDate->getJulianDayNumber())), false);
        $datetime->setDayName($this->nameOfDay($this->dayOfWeek($this->pasoonDate->getJulianDayNumber())), true);
        $datetime->setMonthName($this->monthName($localDate[1]), false);
        $datetime->setMonthName($this->monthName($localDate[1], true), true);
        
        $this->pasoonDate->setTimestamp($utcTimestamp);

        return $datetime;
    }

    public function islamic($year = false, $month = false, $day = false, $hours = false, $minute = false, $second = false)
    {
        return $this->pasoonDate->islamic($year, $month, $day, $hours, $minute, $second);
    }

    public function jalali($year = false, $month = false, $day = false, $hours = false, $minute = false, $second = false)
    {
        return $this->pasoonDate->jalali($year, $month, $day, $hours, $minute, $second);
    }

    public function shia($year = false, $month = false, $day = false, $hours = false, $minute = false, $second = false)
    {
        return $this->pasoonDate->shia($year, $month, $day, $hours, $minute, $second);
    }
}

class IslamicDate extends AbstractPasoonDate
{
    private $ISLAMIC_EPOCH = 1948439.5;
    private $pasoonDate;

    /**
     * @param PasoonDate $pasoonDate
     * @param $year
     * @param $month
     * @param $day
     * @param $hours
     * @param $minute
     * @param $second
     */
    public function __construct($pasoonDate, $year, $month, $day, $hours, $minute, $second)
    {
        $this->pasoonDate = $pasoonDate;
        if ($year and $month and $day) {
            $pasoonDate->setHours(isset($hours) ? $hours : date('H'));
            $pasoonDate->setMinute(isset($minute) ? $minute : date('i'));
            $pasoonDate->setSecond(isset($second) ? $second : date('s'));
            $pasoonDate->setJulianDayNumber($this->toJulianDayNumber($year, $month, $day));
            $pasoonDate->setTimestamp($pasoonDate->getTimestamp() - $pasoonDate->getOffset());
        }
    }

    private function toJulianDayNumber($year, $month, $day)
    {
        return ($day +
            ceil(29.5 * ($month - 1)) +
            ($year - 1) * 354 +
            floor((3 + (11 * $year)) / 30) +
            $this->ISLAMIC_EPOCH) - 1;
    }

    private function toIslamic()
    {
        $jd = $this->pasoonDate->getJulianDayNumber();
        $jd = floor($jd) + 0.5;
        $year = floor(((30 * ($jd - $this->ISLAMIC_EPOCH)) + 10646) / 10631);
        $month = min(12, ceil(($jd - (29 + $this->toJulianDayNumber($year, 1, 1))) / 29.5) + 1);
        $day = $jd - $this->toJulianDayNumber($year, $month, 1) + 1;
        return [$year, $month, $day];
    }

    private function isLeap($year)
    {
        return ((($year * 11) + 14) % 30) < 11;
    }

    public function daysOfMonth($year, $month)
    {
        $countDays = array(30, 29, 30, 29, 30, 29, 30, 29, 30, 29, 30, 29); //30
        if ($month < 1 || $month > 12) {
            throw new Exception("$month Out Of Range Exception");
        }
        if ($year && $this->isLeap($year) && $month == 12) {
            return 30;
        }
        return $countDays[$month - 1];
    }

    public function startDayOfMonth($year, $month)
    {
        return $this->dayOfWeek($this->toJulianDayNumber($year, $month, 1));
    }

    public function lastDayOfMonth($year, $month)
    {
        return $this->dayOfWeek($this->toJulianDayNumber($year, $month, $this->daysOfMonth($year, $month)));
    }

    public function monthName($month)
    {
        $names = ["محرم", "صفر", "ربیع الاول", "ربیع الثانی", "جمادی الاول", "جمادی الثانی", "رجب", "شعبان", "رمضان", "شوال", "ذی‌القعده", "ذی‌الحجه"];
        if (empty($month)) {
            return $names;
        }
        if ($month < 1 || $month > 12) {
            throw new Exception("$month Out Of Range Exception");
        }
        return $names[$month - 1];
    }

    public function nameOfDay($dayOfWeek, $short = false)
    {
        $names = ["الأحد", "الاثنین", "الثلاثاء", "الأربعاء", "الخمیس", "الجمعة", "السبت"];
        $shortNames = ["ح", "ن", "ث", "ر", "خ", "ج", "س"];
        if ($dayOfWeek < 0 || $dayOfWeek > 6) {
            throw new Exception("$dayOfWeek Out Of Range Exception");
        }
        if ($short) {
            return $shortNames[$dayOfWeek];
        }
        return $names[$dayOfWeek];
    }

    /**
     * @return PasoonDate
     */
    public function get()
    {
        $date = $this->toIslamic();
        $datetime = new PasoonDateTime();
        $utcTimestamp = $this->pasoonDate->getTimestamp();
        $utcDate = $this->toIslamic();
        $localTimestamp = $utcTimestamp + $this->pasoonDate->getOffset();
        $localDate;

        $datetime->setTimestamp($utcTimestamp);
        $datetime->setJulianDay($this->pasoonDate->getJulianDayNumber());

        $datetime->setUTCYear($utcDate[0]);
        $datetime->setUTCMonth($utcDate[1]);
        $datetime->setUTCDay($utcDate[2]);
        $datetime->setUTCHour($this->pasoonDate->getHours());
        $datetime->setUTCMinute($this->pasoonDate->getMinute());
        $datetime->setUTCSecond($this->pasoonDate->getSecond());
        $datetime->setUTCDayName($this->nameOfDay($this->dayOfWeek($this->pasoonDate->getJulianDayNumber())), false);
        $datetime->setUTCDayName($this->nameOfDay($this->dayOfWeek($this->pasoonDate->getJulianDayNumber())), true);
        $datetime->setUTCMonthName($this->monthName($utcDate[1]), false);
        $datetime->setUTCMonthName($this->monthName($utcDate[1], true), true);

        $this->pasoonDate->setTimestamp($localTimestamp);
        $localDate = $this->toIslamic();

        $datetime->setTimestamp($localTimestamp);
        $datetime->setJulianDay($this->pasoonDate->getJulianDayNumber());

        $datetime->setYear($localDate[0]);
        $datetime->setMonth($localDate[1]);
        $datetime->setDay($localDate[2]);
        $datetime->setHour($this->pasoonDate->getHours());
        $datetime->setMinute($this->pasoonDate->getMinute());
        $datetime->setSecond($this->pasoonDate->getSecond());
        $datetime->setDayName($this->nameOfDay($this->dayOfWeek($this->pasoonDate->getJulianDayNumber())), false);
        $datetime->setDayName($this->nameOfDay($this->dayOfWeek($this->pasoonDate->getJulianDayNumber())), true);
        $datetime->setMonthName($this->monthName($localDate[1]), false);
        $datetime->setMonthName($this->monthName($localDate[1], true), true);
        
        $this->pasoonDate->setTimestamp($utcTimestamp);

        return $datetime;
    }

    public function gregorian($year = false, $month = false, $day = false, $hours = false, $minute = false, $second = false)
    {
        return $this->pasoonDate->gregorian($year, $month, $day, $hours, $minute, $second);
    }

    public function jalali($year = false, $month = false, $day = false, $hours = false, $minute = false, $second = false)
    {
        return $this->pasoonDate->jalali($year, $month, $day, $hours, $minute, $second);
    }

    public function shia($year = false, $month = false, $day = false, $hours = false, $minute = false, $second = false)
    {
        return $this->pasoonDate->shia($year, $month, $day, $hours, $minute, $second);
    }
}

class ShiaDate extends AbstractPasoonDate
{
    private $ISLAMIC_EPOCH = 1948439.5;
    private $pasoonDate;

    /**
     * @param PasoonDate $pasoonDate
     * @param $year
     * @param $month
     * @param $day
     * @param $hours
     * @param $minute
     * @param $second
     */
    public function __construct($pasoonDate, $year, $month, $day, $hours, $minute, $second)
    {
        $this->pasoonDate = $pasoonDate;
        if ($year and $month and $day) {
            $pasoonDate->setHours(isset($hours) ? $hours : date('H'));
            $pasoonDate->setMinute(isset($minute) ? $minute : date('i'));
            $pasoonDate->setSecond(isset($second) ? $second : date('s'));
            $pasoonDate->setJulianDayNumber($this->toJulianDayNumber($year, $month, $day));
            $pasoonDate->setTimestamp($pasoonDate->getTimestamp() - $pasoonDate->getOffset());
        }
    }

    private function toJulianDayNumber($year, $month, $day)
    {
        $days = 0;
        for ($i = 0; $i < ($month - 1); $i++) {
            $days += $this->daysOfMonth($this, $i + 1);
        }
        return ($day +
            $days +
            ($year - 1) * 354 +
            floor((3 + (11 * $year)) / 30) +
            $this->ISLAMIC_EPOCH) - 1;
    }

    private function toShia()
    {
        $jd = $this->pasoonDate->getJulianDayNumber();
        $jd = floor($jd) + 0.5;
        $year = floor(((30 * ($jd - $this->ISLAMIC_EPOCH)) + 10646) / 10631);
        $month = min(12, ceil(($jd - (29 + $this->toJulianDayNumber($year, 1, 1))) / 29.5) + 1);
        $dayOfYear = $jd - $this->toJulianDayNumber($year, 1, 1);
        $days = 0;
        for ($i = 0; $i < 12; $i++) {
            $days += $this->daysOfMonth($year, $i + 1);
            if ($dayOfYear < $days) {
                $month = $i + 1;
                break;
            }
        }
        $day = ($jd - (($days - $this->daysOfMonth($year, $month)) + ($year - 1) * 354 + floor((3 + (11 * $year)) / 30) + $this->ISLAMIC_EPOCH) + 1);
        return [$year, $month, $day];
    }

    private function isLeap($year)
    {
        return ((($year * 11) + 14) % 30) < 11;
    }

    public function daysOfMonth($year, $month)
    {
        $islamicCountDays = array(30, 29, 30, 29, 30, 29, 30, 29, 30, 29, 30, 29); //30
        $countDays = array(
            1435 => array(29, 30, 29, 30, 29, 30, 29, 30, 30, 30, 29, 30),
            1436 => array(29, 30, 29, 29, 30, 29, 30, 29, 30, 29, 30, 30),
            1437 => array(29, 30, 30, 29, 30, 29, 29, 30, 29, 29, 30, 30),
            1438 => array(29, 30, 30, 30, 29, 30, 29, 29, 30, 29, 29, 30),
            1439 => array(29, 30, 30, 30, 30, 29, 30, 29, 29, 30, 29, 29),
            1440 => array(30, 29, 30, 30, 30, 29, 29, 30, 29, 30, 29, 29),
        );
        if ($month < 1 || $month > 12) {
            throw new \Exception("$month Out Of Range Exception");
        }

        if (empty($countDays[$year])) {
            if ($year && $this->isLeap($year) && $month == 12) {
                return 30;
            }
            return $islamicCountDays[$month - 1];
        }
        
        
        return $countDays[$year][$month - 1];
    }

    public function startDayOfMonth($year, $month)
    {
        return $this->dayOfWeek($this->toJulianDayNumber($year, $month, 1));
    }

    public function lastDayOfMonth($year, $month)
    {
        return $this->dayOfWeek($this->toJulianDayNumber($year, $month, $this->daysOfMonth($year, $month)));
    }

    public function monthName($month)
    {
        $names = ["محرم", "صفر", "ربیع الاول", "ربیع الثانی", "جمادی الاول", "جمادی الثانی", "رجب", "شعبان", "رمضان", "شوال", "ذی‌القعده", "ذی‌الحجه"];
        if (empty($month)) {
            return $names;
        }
        if ($month < 1 || $month > 12) {
            throw new Exception("$month Out Of Range Exception");
        }
        return $names[$month - 1];
    }

    public function nameOfDay($dayOfWeek, $short = false)
    {
        $names = ["الأحد", "الاثنین", "الثلاثاء", "الأربعاء", "الخمیس", "الجمعة", "السبت"];
        $shortNames = ["ح", "ن", "ث", "ر", "خ", "ج", "س"];
        if ($dayOfWeek < 0 || $dayOfWeek > 6) {
            throw new Exception("$dayOfWeek Out Of Range Exception");
        }
        if ($short) {
            return $shortNames[$dayOfWeek];
        }
        return $names[$dayOfWeek];
    }

    public function get()
    {
        $date = $this->toShia();
        $datetime = new PasoonDateTime();
        $utcTimestamp = $this->pasoonDate->getTimestamp();
        $utcDate = $this->toShia();
        $localTimestamp = $utcTimestamp + $this->pasoonDate->getOffset();
        $localDate;

        $datetime->setTimestamp($utcTimestamp);
        $datetime->setJulianDay($this->pasoonDate->getJulianDayNumber());

        $datetime->setUTCYear($utcDate[0]);
        $datetime->setUTCMonth($utcDate[1]);
        $datetime->setUTCDay($utcDate[2]);
        $datetime->setUTCHour($this->pasoonDate->getHours());
        $datetime->setUTCMinute($this->pasoonDate->getMinute());
        $datetime->setUTCSecond($this->pasoonDate->getSecond());
        $datetime->setUTCDayName($this->nameOfDay($this->dayOfWeek($this->pasoonDate->getJulianDayNumber())), false);
        $datetime->setUTCDayName($this->nameOfDay($this->dayOfWeek($this->pasoonDate->getJulianDayNumber())), true);
        $datetime->setUTCMonthName($this->monthName($utcDate[1]), false);
        $datetime->setUTCMonthName($this->monthName($utcDate[1], true), true);

        $this->pasoonDate->setTimestamp($localTimestamp);
        $localDate = $this->toShia();

        $datetime->setTimestamp($localTimestamp);
        $datetime->setJulianDay($this->pasoonDate->getJulianDayNumber());

        $datetime->setYear($localDate[0]);
        $datetime->setMonth($localDate[1]);
        $datetime->setDay($localDate[2]);
        $datetime->setHour($this->pasoonDate->getHours());
        $datetime->setMinute($this->pasoonDate->getMinute());
        $datetime->setSecond($this->pasoonDate->getSecond());
        $datetime->setDayName($this->nameOfDay($this->dayOfWeek($this->pasoonDate->getJulianDayNumber())), false);
        $datetime->setDayName($this->nameOfDay($this->dayOfWeek($this->pasoonDate->getJulianDayNumber())), true);
        $datetime->setMonthName($this->monthName($localDate[1]), false);
        $datetime->setMonthName($this->monthName($localDate[1], true), true);
        
        $this->pasoonDate->setTimestamp($utcTimestamp);

        return $datetime;
    }

    public function gregorian($year = false, $month = false, $day = false, $hours = false, $minute = false, $second = false)
    {
        return $this->pasoonDate->gregorian($year, $month, $day, $hours, $minute, $second);
    }

    public function jalali($year = false, $month = false, $day = false, $hours = false, $minute = false, $second = false)
    {
        return $this->pasoonDate->jalali($year, $month, $day, $hours, $minute, $second);
    }

    public function islamic($year = false, $month = false, $day = false, $hours = false, $minute = false, $second = false)
    {
        return $this->pasoonDate->islamic($year, $month, $day, $hours, $minute, $second);
    }
}

class JalaliDate extends AbstractPasoonDate
{
    private $PERSIAN_EPOCH = 1948320.5;
    private $pasoonDate;

    /**
     * @param PasoonDate $pasoonDate
     * @param $year
     * @param $month
     * @param $day
     * @param $hours
     * @param $minute
     * @param $second
     */
    public function __construct($pasoonDate, $year, $month, $day, $hours, $minute, $second)
    {
        $this->pasoonDate = $pasoonDate;
        if ($year and $month and $day) {
            $pasoonDate->setHours(isset($hours) ? $hours : date('H'));
            $pasoonDate->setMinute(isset($minute) ? $minute : date('i'));
            $pasoonDate->setSecond(isset($second) ? $second : date('s'));
            $pasoonDate->setJulianDayNumber($this->toJulianDayNumber($year, $month, $day));
            $pasoonDate->setTimestamp($pasoonDate->getTimestamp() - $pasoonDate->getOffset());
        }
    }

    private function toJulianDayNumber($year, $month, $day)
    {
        $epbase = $year - (($year >= 0) ? 474 : 473);
        $epyear = 474 + $this->mod($epbase, 2820);

        return $day +
        (($month <= 7) ? (($month - 1) * 31) : ((($month - 1) * 30) + 6)) +
        floor((($epyear * 682) - 110) / 2816) +
        ($epyear - 1) * 365 +
        floor($epbase / 2820) * 1029983 +
        ($this->PERSIAN_EPOCH - 1);
    }

    private function toJalali()
    {
        $jd = $this->pasoonDate->getJulianDayNumber();
        $year = $month = $day = $depoch = $cycle = $cyear = $ycycle = $aux1 = $aux2 = $yday = 0;

        $jd = floor($jd) + 0.5;

        $depoch = $jd - $this->toJulianDayNumber(475, 1, 1);
        $cycle = floor($depoch / 1029983);
        $cyear = $this->mod($depoch, 1029983);
        if ($cyear == 1029982) {
            $ycycle = 2820;
        } else {
            $aux1 = floor($cyear / 366);
            $aux2 = $this->mod($cyear, 366);
            $ycycle = floor(((2134 * $aux1) + (2816 * $aux2) + 2815) / 1028522) +
                $aux1 + 1;
        }
        $year = $ycycle + (2820 * $cycle) + 474;
        if ($year <= 0) {
            $year--;
        }
        $yday = ($jd - $this->toJulianDayNumber($year, 1, 1)) + 1;
        $month = ($yday <= 186) ? ceil($yday / 31) : ceil(($yday - 6) / 30);
        $day = ($jd - $this->toJulianDayNumber($year, $month, 1)) + 1;
        return [$year, $month, $day];
    }

    private function isLeap($year)
    {
        return (((((($year - (($year > 0) ? 474 : 473)) % 2820) + 474) + 38) * 682) % 2816) < 682;
    }


    public function daysOfMonth($year, $month)
    {
        $countDays = array(31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29);
        if ($month < 1 || $month > 12) {
            throw new Exception("$month Out Of Range Exception");
        }
        if ($year && $this->isLeap($year) && $month == 12) {
            return 30;
        }
        return $countDays[$month - 1];
    }

    public function startDayOfMonth($year, $month)
    {
        return $this->dayOfWeek($this->toJulianDayNumber($year, $month, 1));
    }

    public function lastDayOfMonth($year, $month)
    {
        return $this->dayOfWeek($this->toJulianDayNumber($year, $month, $this->daysOfMonth($year, $month)));
    }

    public function monthName($month)
    {
        $names = ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند'];
        if (empty($month)) {
            return $names;
        }
        if ($month < 1 || $month > 12) {
            throw new Exception("$month Out Of Range Exception");
        }
        return $names[$month - 1];
    }

    public function nameOfDay($dayOfWeek, $short = false)
    {
        $names = ["یک شنبه", "دوشنبه", "سه شنبه", "چهارشنبه", "پنج شنبه", "جمعه", "شنبه"];
        if ($dayOfWeek < 0 || $dayOfWeek > 6) {
            throw new Exception("$dayOfWeek Out Of Range Exception");
        }
        if ($short) {
            return substr($names[$dayOfWeek], 0, 2);
        }
        return $names[$dayOfWeek];
    }

    public function get()
    {
        $date = $this->toJalali();
        $datetime = new PasoonDateTime();
        $utcTimestamp = $this->pasoonDate->getTimestamp();
        $utcDate = $this->toJalali();
        $localTimestamp = $utcTimestamp + $this->pasoonDate->getOffset();
        $localDate;

        $datetime->setTimestamp($utcTimestamp);
        $datetime->setJulianDay($this->pasoonDate->getJulianDayNumber());

        $datetime->setUTCYear($utcDate[0]);
        $datetime->setUTCMonth($utcDate[1]);
        $datetime->setUTCDay($utcDate[2]);
        $datetime->setUTCHour($this->pasoonDate->getHours());
        $datetime->setUTCMinute($this->pasoonDate->getMinute());
        $datetime->setUTCSecond($this->pasoonDate->getSecond());
        $datetime->setUTCDayName($this->nameOfDay($this->dayOfWeek($this->pasoonDate->getJulianDayNumber())), false);
        $datetime->setUTCDayName($this->nameOfDay($this->dayOfWeek($this->pasoonDate->getJulianDayNumber())), true);
        $datetime->setUTCMonthName($this->monthName($utcDate[1]), false);
        $datetime->setUTCMonthName($this->monthName($utcDate[1], true), true);

        $this->pasoonDate->setTimestamp($localTimestamp);
        $localDate = $this->toJalali();

        $datetime->setTimestamp($localTimestamp);
        $datetime->setJulianDay($this->pasoonDate->getJulianDayNumber());

        $datetime->setYear($localDate[0]);
        $datetime->setMonth($localDate[1]);
        $datetime->setDay($localDate[2]);
        $datetime->setHour($this->pasoonDate->getHours());
        $datetime->setMinute($this->pasoonDate->getMinute());
        $datetime->setSecond($this->pasoonDate->getSecond());
        $datetime->setDayName($this->nameOfDay($this->dayOfWeek($this->pasoonDate->getJulianDayNumber())), false);
        $datetime->setDayName($this->nameOfDay($this->dayOfWeek($this->pasoonDate->getJulianDayNumber())), true);
        $datetime->setMonthName($this->monthName($localDate[1]), false);
        $datetime->setMonthName($this->monthName($localDate[1], true), true);
        
        $this->pasoonDate->setTimestamp($utcTimestamp);

        return $datetime;
    }

    public function gregorian($year = false, $month = false, $day = false, $hours = false, $minute = false, $second = false)
    {
        return $this->pasoonDate->gregorian($year, $month, $day, $hours, $minute, $second);
    }

    public function islamic($year = false, $month = false, $day = false, $hours = false, $minute = false, $second = false)
    {
        return $this->pasoonDate->islamic($year, $month, $day, $hours, $minute, $second);
    }

    public function shia($year = false, $month = false, $day = false, $hours = false, $minute = false, $second = false)
    {
        return $this->pasoonDate->shia($year, $month, $day, $hours, $minute, $second);
    }
}

class PasoonDateTime
{
    private $timestamp;
    private $offset;
    private $julianDay;

    private $year;
    private $month;
    private $day;
    private $hour;
    private $minute;
    private $second;

    private $utcYear;
    private $utcMonth;
    private $utcDay;
    private $utcHour;
    private $utcMinute;
    private $utcSecond;

    private $dayName;
    private $shortDayName;
    private $monthName;
    private $shortMonthName;

    private $utcDayName;
    private $utcShortDayName;
    private $utcMonthName;
    private $utcShortMonthName;


    public function __construct()
    {
        
    }

    public function setTimestamp($value)
    {
        $this->timestamp = (integer)$value;
    }

    public function getTimestamp()
    {
        return $this->timestamp;
    }

    public function setOffset($value)
    {
        $this->offset = (integer)$value;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    public function setJulianDay($value)
    {
        $this->julianDay = $value;
    }

    public function getJulianDay()
    {
        return $this->julianDay;
    }

    public function setYear($value)
    {
        $this->year = (integer)$value;
    }
    
    public function getYear()
    {
        return $this->year;
    }

    public function setMonth($value)
    {
        $this->month = (integer)$value;
    }
    
    public function getMonth()
    {
        return $this->month;
    }

    public function setDay($value)
    {
        $this->day = (integer)$value;
    }
    
    public function getDay()
    {
        return $this->day;
    }

    public function setHour($value)
    {
        $this->hour = (integer)$value;
    }

    public function getHour()
    {
        return $this->hour;
    }

    public function setMinute($value)
    {
        $this->minute = (integer)$value;
    }

    public function getMinute()
    {
        return $this->minute;
    }

    public function setSecond($value)
    {
        $this->second = (integer)$value;
    }

    public function getSecond()
    {
        return $this->second;
    }

    public function setDayName($value, $short = false)
    {
        if($short) {
            $this->shortDayName = $value;
        }
        else {
            $this->dayName = $value;
        }
    }

    public function getDayName($short = false)
    {
        if ($short)
            return $this->shortDayName;
        return $this->dayName;
    }

    public function setMonthName($value, $short = false)
    {
        if($short) {
            $this->shortMonthName = $value;
        }
        else {
            $this->monthName = $value;
        }
    }

    public function getMonthName($short = false)
    {
        if ($short)
            return $this->shortMonthName;
        return $this->monthName;
    }

    public function setUTCYear($value)
    {
        $this->utcYear = (integer)$value;
    }
    
    public function getUTCYear()
    {
        return $this->utcYear;
    }

    public function setUTCMonth($value)
    {
        $this->utcMonth = (integer)$value;
    }
    
    public function getUTCMonth()
    {
        return $this->utcMonth;
    }

    public function setUTCDay($value)
    {
        $this->utcDay = (integer)$value;
    }
    
    public function getUTCDay()
    {
        return $this->utcDay;
    }

    public function setUTCHour($value)
    {
        $this->utcHour = (integer)$value;
    }

    public function getUTCHour()
    {
        return $this->utcHour;
    }

    public function setUTCMinute($value)
    {
        $this->utcMinute = (integer)$value;
    }

    public function getUTCMinute()
    {
        return $this->utcMinute;
    }

    public function setUTCSecond($value)
    {
        $this->utcSecond = (integer)$value;
    }

    public function getUTCSecond()
    {
        return $this->utcSecond;
    }

    public function setUTCDayName($value, $short = false)
    {
        if($short) {
            $this->utcShortDayName = $value;
        }
        else {
            $this->utcDayName = $value;
        }
    }

    public function getUTCDayName($short = false)
    {
        if ($short)
            return $this->utcShortDayName;
        return $this->utcDayName;
    }

    public function setUTCMonthName($value, $short = false)
    {
        if($short) {
            $this->utcShortMonthName = $value;
        }
        else {
            $this->utcMonthName = $value;
        }
    }

    public function getUTCMonthName($short = false)
    {
        if ($short)
            return $this->utcShortMonthName;
        return $this->utcMonthName;
    }

    /**
     * @param $pattern
     * @return string
     */
    public function format($pattern)
    {
        $temp = $pattern;
        $temp = str_replace('%Day%', $this->leadingZeros($this->day), $temp);
        $temp = str_replace('%day%', $this->day, $temp);
        $temp = str_replace(array('%day:name:short%', '%Day:name:short%'), $this->shortDayName, $temp);
        $temp = str_replace(array('%day:name%', '%Day:name%'), $this->dayName, $temp);

        $temp = str_replace(array('%month:name%', '%Month:name%'), $this->monthName, $temp);
        $temp = str_replace(array('%month:name:short%', '%Month:name:short%'), $this->shortMonthName, $temp);
        $temp = str_replace('%Month%', $this->leadingZeros($this->month), $temp);
        $temp = str_replace('%month%', $this->month, $temp);

        $temp = str_replace('%Year%', $this->year, $temp);
        $temp = str_replace('%year%', substr($this->year, 2), $temp);

        $temp = str_replace('meridiem', ($this->hour > 12 ? 'pm' : 'am'), $temp);
        $temp = str_replace('Meridiem', ($this->hour > 12 ? 'PM' : 'pm'), $temp);

        $temp = str_replace('%hour:12%', ($this->hour > 12 ? $this->hour - 12 : $this->hour), $temp);
        $temp = str_replace('%hour%', $this->hour, $temp);
        $temp = str_replace('%Hour:12%', $this->leadingZeros(($this->hour > 12 ? $this->hour - 12 : $this->hour)), $temp);
        $temp = str_replace('%Hour%', $this->leadingZeros($this->hour), $temp);
        $temp = str_replace('%minute%', $this->minute, $temp);
        $temp = str_replace('%Minute%', $this->leadingZeros($this->minute), $temp);
        $temp = str_replace('%second%', $this->second, $temp);
        $temp = str_replace('%Second%', $this->leadingZeros($this->second), $temp);


        $temp = str_replace('%UDay%', $this->leadingZeros($this->utcDay), $temp);
        $temp = str_replace('%uday%', $this->utcDay, $temp);
        $temp = str_replace(array('%uday:name:short%', '%UDay:name:short%'), $this->utcShortDayName, $temp);
        $temp = str_replace(array('%uday:name%', '%UDay:name%'), $this->utcDayName, $temp);

        $temp = str_replace(array('%umonth:name%', '%UMonth:name%'), $this->utcMonthName, $temp);
        $temp = str_replace(array('%umonth:name:short%', '%UMonth:name:short%'), $this->utcShortMonthName, $temp);
        $temp = str_replace('%UMonth%', $this->leadingZeros($this->utcMonth), $temp);
        $temp = str_replace('%umonth%', $this->utcMonth, $temp);

        $temp = str_replace('%UYear%', $this->utcYear, $temp);
        $temp = str_replace('%uyear%', substr($this->utcYear, 2), $temp);

        $temp = str_replace('umeridiem', ($this->utcHour > 12 ? 'pm' : 'am'), $temp);
        $temp = str_replace('UMeridiem', ($this->utcHour > 12 ? 'PM' : 'pm'), $temp);

        $temp = str_replace('%uhour:12%', ($this->utcHour > 12 ? $this->utcHour - 12 : $this->utcHour), $temp);
        $temp = str_replace('%uhour%', $this->utcHour, $temp);
        $temp = str_replace('%UHour:12%', $this->leadingZeros(($this->utcHour > 12 ? $this->utcHour - 12 : $this->utcHour)), $temp);
        $temp = str_replace('%UHour%', $this->leadingZeros($this->utcHour), $temp);
        $temp = str_replace('%uminute%', $this->utcMinute, $temp);
        $temp = str_replace('%UMinute%', $this->leadingZeros($this->utcMinute), $temp);
        $temp = str_replace('%usecond%', $this->utcSecond, $temp);
        $temp = str_replace('%USecond%', $this->leadingZeros($this->utcSecond), $temp);

        return $temp;
    }

    /**
     * @return array
     */
    public function getArray()
    {
        return array(
            'year' => $this->getYear(),
            'month' => $this->getMonth(),
            'day' => $this->getDay(),
            'hour' => $this->getHour(),
            'minute' => $this->getMinute(),
            'second' => $this->getSecond(),
            'day name' => $this->getDayName(),
            'month name' => $this->getMonthName(),
            'utc year' => $this->getUTCYear(),
            'utc month' => $this->getUTCMonth(),
            'utc day' => $this->getUTCDay(),
            'utc hour' => $this->getUTCHour(),
            'utc minute' => $this->getUTCMinute(),
            'utc second' => $this->getUTCSecond(),
            'utc day name' => $this->getUTCDayName(),
            'utc month name' => $this->getUTCMonthName(),
            'timestamp' => $this->getTimestamp(),
            'offset' => $this->getOffset(),
            'julian day' => $this->getJulianDay()
        );
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return "{$this->getYear()}-{$this->getMonth()}-{$this->getDay()} {$this->getHour()}:{$this->getMinute()}:{$this->getSecond()}";
    }

    private function leadingZeros($number)
    {
        return sprintf('%02d', $number);
    }
}