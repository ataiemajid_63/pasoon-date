<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script type="text/javascript" src="js/pasoon-date.js"></script>
    <!--<script type="text/javascript" src="js/pasoon-date-timezone.js"></script>-->
    <!--<script type="text/javascript" src="dist/pasoon-date.min.js"></script>-->
</head>
<body>
<?php

require_once('php/PasoonDate.php');

use Pasoon\PasoonDate;

$pasoonDate = new PasoonDate('now', 3.5 * 3600);

echo '<h4>Jalali: </h4>';
echo $pasoonDate->jalali()->get()->format("%UYear%/%UMonth%/%UDay% %UHour%:%UMinute%:%USecond% | %Year%/%Month%/%Day% %Hour%:%Minute%:%Second%");
echo '<h4>Shia: </h4>';
echo $pasoonDate->shia()->get()->format("%UYear%/%UMonth%/%UDay% %UHour%:%UMinute%:%USecond% | %Year%/%Month%/%Day% %Hour%:%Minute%:%Second%");
echo '<h4>Islamic: </h4>';
echo $pasoonDate->islamic()->get()->format("%UYear%/%UMonth%/%UDay% %UHour%:%UMinute%:%USecond% | %Year%/%Month%/%Day% %Hour%:%Minute%:%Second%");
echo '<h4>Gregorian: </h4>';
echo $pasoonDate->gregorian()->get()->format("%UYear%/%UMonth%/%UDay% %UHour%:%UMinute%:%USecond% | %Year%/%Month%/%Day% %Hour%:%Minute%:%Second%");

?>
</body>
</html>
